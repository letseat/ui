# LetsEat! Frontend 


## Build Setup

``` bash
# install dependencies
yarn  install

# serve with hot reload at localhost:8080
yarn run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
yarn run build --report
```
