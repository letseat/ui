import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import PlaceDetails from '@/components/PlaceDetails'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/:id/:name',
      name: 'PlaceDetails',
      component: PlaceDetails
    }
  ]
})
