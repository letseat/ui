import Vue from 'vue'

export default {
  token: 'LWFBV3C5UIEZF5NLGAIRQ0G1GRC4HAGYPYS3SA4PQIKGZF12&v=20170625',
  list (near) {
    let api = 'https://api.foursquare.com/v2/venues/search'
    let categoryId = '4d4b7105d754a06374d81259'
    return Vue.http.get(`${api}?near=${near}&oauth_token=${this.token}&categoryId=${categoryId}`)
    // return Vue.http.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=AIzaSyBPsCyXQ3HdI12eiN73NMxtZUKj7ZYhPow')
  },
  get (id) {
    let api = `https://api.foursquare.com/v2/venues/${id}?oauth_token=${this.token}`
    return Vue.http.get(api)
  },
  listCategories () {
    let api = `https://api.foursquare.com/v2/venues/categories?oauth_token=${this.token}`
    return Vue.http.get(api).then(res => {
      return Promise.resolve(
        res.body.response.categories.filter(category => category.name === 'Food' ? category : null)[0].categories
      )
    })
  }
}
