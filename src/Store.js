import Vue from 'vue'
import Vuex from 'vuex'
import PlacesService from './services/Places'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    nearby: 'Rotterdam,NL',
    places: {},
    categories: []
  },
  mutations: {
    setNearby (val) {
      this.state.nearby = val
    },
    setPlaces (nearby, places) {
      this.state.places[nearby] = places
    },
    setCategories (categories) {
      this.state.categories = categories
    }
  },
  actions: {
    fetchPlaces (context, nearby) {
      if (context.state.places[nearby]) {
        return Promise.resolve(context.state.places[nearby])
      }
      return PlacesService.list(nearby).then(res => {
        context.state.places[nearby] = res.body.response.venues
        return Promise.resolve(res.body.response.venues)
      })
    },
    fetchCategories (context) {
      PlacesService.listCategories().then(res => {
        context.state.categories = res
      })
    }
  }
})
